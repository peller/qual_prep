\documentclass{article}

\newif\ifonlythm
\onlythmfalse
\newif\ifonlydef
\onlydeffalse

\usepackage{notes}
\usepackage{listings}
\usepackage[margin=0.75in]{geometry}

\begin{document}

\section{Qual Exam General Information}

\subsection{General Description}
\begin{empty}
The Numerical Analysis PhD Qualifying Examination (NA Qual) is intended to determine whether a student is qualified to begin research toward a PhD dissertation in the general area of numerical analysis. The NA Qual also enforces a breadth requirement in numerical analysis. The format is a 90-minute oral examination in which a committee of three faculty members asks the student questions on any topic listed in the examination syllabus. The student is expected to be able to supply any requested details, such as proofs and derivations, on the whiteboard without consulting any notes or references.

Prior to taking the Qual, the student must submit a written statement (one to two pages) describing his or her academic background, technical interests, experience, intended area of research specialization, and general academic plans. The student begins the examination with a brief (five to ten minute) oral summary of his or her research interests. The committee may ask a few questions specific to the student's stated interests, but by far the bulk of the examination consists of general questions drawn from the material listed in the examination syllabus.

There are numerous facts and techniques that should be known by every PhD graduate who claims a research interest in numerical analysis. A 90-minute examination is inadequate to test all of these, but a reasonable subset is to focus on the material typically covered in the courses CS 450 and CS 555, which serves as the basis for the detailed examination syllabus. Note that the student is responsible for all of the topics in the examination syllabus, even if some may have been omitted from a particular offering of CS 450 or 555. 
\end{empty}

\subsection{Advice to the Student}
\begin{empty}
\begin{itemize}
\item You should take both CS 450 and CS 555 unless you have already taken equivalent courses elsewhere and done well in them.
\item You should obtain several intermediate-level books on numerical analysis and use them to flesh out your knowledge of the topics listed in the syllabus (specific suggestions are given in the accompanying reading list). There is often more than one way to express various definitions, theorems, and algorithms, and you should find ways of expressing them that you are comfortable with.
\item You should confer with your faculty advisor, who can give you a realistic assessment of your potential as well as specific advice on exam preparation.
\item You should practice as much as possible by answering the sample questions and questions from previous NA Qual exams, and by conducting mock quals with fellow students and/or your advisor. The latter will give you experience answering questions "on your feet" in a setting that approximates that of the actual exam, and will help you become more comfortable with the exam format. 
\end{itemize}
\end{empty}

\subsection{Exam Syllabus}
\begin{empty}
The student is expected to have a general knowledge of the following topics at about the level covered in the lectures, lecture notes, textbooks, and prerequisites for CS 450 and CS 555. For each topic (concept, theorem, problem, algorithm, etc.) listed, standard questions that might be asked include its definition, existence, uniqueness, characterization, derivation, proof, applicability, sensitivity, stability, accuracy, convergence, computational complexity, etc., as may be relevant. 
\end{empty}

\section{Topics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numerical Computation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Numerical Computation}

\begin{itemize}
\item General error analysis: well-posed problems, problems vs. algorithms, data error vs. computational error, forward error vs. backward error, conditioning of a problem, stability of an algorithm.
\item Finite-precision computation: floating-point numbers, rounding rules, machine precision, floating-point arithmetic, cancellation.
\item Operation counts, "big O" notation. 
\end{itemize}

\subsection{Systems of Linear Equations}

\begin{itemize}
\item Mathematical background: vector spaces, subspaces, linear independence, rank, dimension, span, basis, null space, determinant, existence and uniqueness of solutions to linear systems, permutation matrices, transpose, conjugate transpose, symmetric matrices, Hermitian matrices, partitioned matrices.
\item Norms and conditioning: vector norm, matrix norm, condition number of a matrix, error bounds for solutions to linear systems, residual vs. error.
\item Gaussian elimination: LU factorization, permutations, partial pivoting, complete pivoting, growth factor, scaling, diagonal dominance, iterative refinement, Gauss-Jordan elimination, rank-one updating.
\item Symmetric matrices: symmetric positive definite matrices, Cholesky factorization, symmetric indefinite systems.
\item Sparse linear systems: band matrices, sparse storage schemes, graph of a matrix, graph interpretation of elimination, fill, reordering for sparsity, nested dissection, minimum degree.
\item Stationary iterative methods: spectral radius, convergence rate, Jacobi, Gauss-Seidel, and SOR methods.
\item Conjugate gradient method: finite convergence, optimality, conjugacy of search directions. 
\end{itemize}

\subsection{Linear Least Squares Problems}

\begin{itemize}
\item Mathematical background: orthogonality, idempotence, orthogonal projectors, existence and uniqueness of least squares solutions to (not necessarily square) linear systems, normal equations.
\item Orthogonalization methods: orthogonal matrices, Householder reflections, Givens rotations, QR factorization, classical and modified Gram-Schmidt orthogonalization.
\item Singular value decomposition: rank, Euclidean norm, condition number, and pseudoinverse of a matrix in terms of its SVD; orthonormal bases, lower-rank approximation. 
\end{itemize}

\subsection{Algebraic Eigenvalue Problems}

\begin{itemize}
\item Mathematical background: eigenvalues, characteristic polynomial, Cayley-Hamilton Theorem, algebraic and geometric multiplicity, defective matrix, diagonalizable matrices, normal matrices, invariant subspaces, block triangular matrices, Schur form, real Schur form, Jordan form, Gershgorin Theorem.
\item Power method, inverse iteration, Raleigh quotient iteration, deflation.
\item QR iteration: simultaneous iteration, orthogonal iteration, QR iteration, reduction to Hessenberg or tridiagonal form, shifts.
\item Krylov subspace methods: Lanczos iteration, Arnoldi iteration
\item Jacobi iteration 
\end{itemize}

\subsection{Nonlinear Equations and Optimization}

\begin{itemize}
\item Mathematical background: Intermediate Value Theorem, Inverse Function Theorem, Contraction Mapping Theorem, coerciveness, convexity, gradient vector, Hessian matrix, optimality conditions.
\item Convergence rates, order of convergence, simple vs. multiple root.
\item Single equations: bisection, fixed-point iteration, Newton's method, secant method, inverse interpolation, safeguarded methods.
\item Systems of equations: fixed-point iteration, Jacobian matrix, Newton's method, Broyden's method, robust (damped or trust-region) Newton-like methods.
\item One-dimensional optimization: golden section search, successive parabolic interpolation, Newton's method, safeguarded methods.
\item Multidimensional unconstrained optimization: steepest descent method, Newton's method, quasi-Newton methods, conjugate gradient method, line search, trust-region methods. 
\end{itemize}

\subsection{Interpolation and Approximation}

\begin{itemize}
\item Mathematical background: existence and uniqueness of interpolant.
\item Polynomial interpolation: Vandermonde matrix, Lagrange and Newton forms, divided differences, Hermite interpolation, convergence and error bound for polynomial interpolation of a continuous function, Runge phenomenon, Chebyshev points, Taylor polynomial.
\item Piecewise polynomial interpolation: Hermite cubic interpolation, splines, cubic spline interpolation, natural cubic spline.
\item Trigonometric interpolation: discrete Fourier transform, FFT algorithm
\item Orthogonal polynomials: function spaces, inner products, orthogonality, Gram-Schmidt orthogonalization, three-term recurrence, Legendre polynomials, Chebyshev polynomials.
\item Polynomial approximation: Weierstass Approximation Theorem, best uniform approximation, least squares approximation. 
\end{itemize}

\subsection{Numerical Integration and Differentiation}

\begin{itemize}
\item Mathematical background: Riemann sums, existence of Riemann integral, ill-posedness of differentiation.
\item Numerical quadrature: interpolatory quadrature, method of undetermined coefficients, moment equations, Newton-Cotes quadrature, midpoint rule, trapezoid rule, Simpson's rule, Gaussian quadrature, composite quadrature, adaptive quadrature.
\item Extrapolation methods: Richardson extrapolation, Romberg integration
\item Numerical differentiation: conditioning, interpolation, smoothing, finite difference approximations. 
\end{itemize}

\subsection{Initial Value Problems for ODEs}

\begin{itemize}
\item Mathematical background: order, reduction to first order, autonomous systems, stability of solutions, linear homogeneous systems with constant coefficients, Jacobian matrix for a nonlinear system.
\item Basic numerical methods: Euler's method, backward Euler method, trapezoid method.
\item Accuracy and stability: truncation error, local error, global error, order of accuracy, stability of numerical method, scalar test problem, convergence, difference equations, root condition, local error estimation, step size control.
\item Multistep methods: explicit and implicit Adams methods, predictor-corrector methods, backward differentiation formulas, solution of implicit equations.
\item Taylor series, Runge-Kutta, and extrapolation methods. 
\end{itemize}

\subsection{Boundary Value Problems for ODEs}

\begin{itemize}
\item Mathematical background: fundamental matrix, Green's function, dichotomy.
\item Shooting method
\item Finite difference method
\item Collocation method
\item Galerkin method 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numerical Solution of Elliptic PDEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Numerical Solution of Elliptic PDEs}

\begin{empty}
\begin{itemize}
\item Mathematical background: Laplace, Poisson, and Helmholtz equations; essential (Dirichlet) and natural (Neumann) boundary conditions.
\item Weighted residual methods: weak form, trial and test functions; Divergence Theorem and integration by parts, Rayleigh-Ritz-Galerkin method, stiffness matrix and load vector.
\item Finite element methods: linear and quadratic finite elements, shape functions, basis functions, quadrature, assembly.
\item Finite difference methods: five-point stencil, symmetry and positive definiteness, fast direct solvers (e.g., cyclic reduction). 
\end{itemize}
\end{empty}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numerical Solution of Parabolic PDEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Numerical Solution of Parabolic PDEs}

\begin{empty}
\begin{itemize}
\item Mathematical background: heat equation in one and two space dimensions, boundary conditions, advection-diffusion equation, Burgers' equation, ill-posedness of backward heat equation.
\item Numerical methods: theta method, Crank-Nicolson method, method of lines, ADI.
\item Accuracy and stability: local and global truncation error, Fourier (von Neumann) stability analysis.
\item Linearization of nonlinear discrete equations by Newton's method. 
\end{itemize}
\end{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numerical Solution of Hyperbolic PDEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Numerical Solution of Hyperbolic PDEs}

\begin{empty}
\begin{itemize}
\item Mathematical background: classification of first-order systems and second-order single equations, characteristics, domain of dependence.
\item Conservation laws: advection equation, wave equation, inviscid Burgers' equation.
\item Finite difference methods: consistency, stability, convergence, Lax Equivalence Theorem, Fourier (von Neumann) stability analysis. 
\end{itemize}
\end{empty}

\end{document}
